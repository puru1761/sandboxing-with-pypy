This repository is submission for the Lab 4 Assignment of course EN.600.424 Network Security.

We have implemented the following capabilities:

- Open and write to a file in "w", "w+" and "a" mode
- Create new file if it doesn't exist and the file is opened in any of the following modes: "w", "w+" and "a"; aka, "wb", "ab", "wb+" and "ab+"
- Create new directories within the virtual file system using os.mkdir
- Navigate between directories using os.chdir


This submission represents the team members:

- Praveen Malhan
- Purushottam Kulkarni
- Ritvik Sachdev
- Vedasagar Karthikayen
